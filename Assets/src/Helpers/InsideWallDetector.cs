using System;
using UnityEngine;

namespace src {
  public class InsideWallDetector: MonoBehaviour {
    public bool isInWall = false;
    
    private void OnTriggerEnter(Collider other) {
      if (other.name.Contains("Cover")) {
        isInWall = true;
      }
    }

    private void OnTriggerExit(Collider other) {
      if (other.name.Contains("Cover")) {
        isInWall = false;
      }
    }
  }
}