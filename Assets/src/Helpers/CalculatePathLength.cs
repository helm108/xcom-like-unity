using System;
using UnityEngine;
using UnityEngine.AI;

namespace src.Helpers {
  public static class CalculatePathLength {
    public static int GetPathLength(NavMeshPath path) {
      var length = 0.0f;

      if ((path.status == NavMeshPathStatus.PathInvalid) || (path.corners.Length <= 1)) {
        return 0;
      };
      
      for (var i = 1; i < path.corners.Length; ++i) {
        length += Vector3.Distance(path.corners[i - 1], path.corners[i]);
      }

      return (int)Math.Round(length);
    }
  }
}