using System;
using UnityEngine;

namespace src {
  public class TileDetector: MonoBehaviour {
    public bool isOccupied;
    
    private void OnTriggerEnter(Collider other) {
      if (other.name.Contains("Soldier")) {
        isOccupied = true;
      }
    }

    private void OnTriggerExit(Collider other) {
      if (other.name.Contains("Soldier")) {
        isOccupied = false;
      }
    }
  }
}