using UnityEngine;

namespace src {
  public class MouseOver: MonoBehaviour {
    public GameObject scriptHandler;
    private EventManager eventManager;
    private readonly Color mouseOverColor = Color.grey;
    private Color originalColor;
    private MeshRenderer meshRenderer;

    public void Start() {
      eventManager = scriptHandler.GetComponent<EventManager>();
      meshRenderer = GetComponent<MeshRenderer>();
      originalColor = meshRenderer.material.color;
    }

    public void OnMouseOver() {
      if (gameObject.name.Contains("Tile")) {
        EventManager.SetCurrentTile(gameObject);
      }
      
      if (gameObject.name.Contains("Cover")) {
        meshRenderer.material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
      }
    }

    public void OnMouseExit() {
      if (gameObject.name.Contains("Tile")) {
        meshRenderer.material.color = originalColor;
        EventManager.ClearCurrentTile();
      }
      
      if (gameObject.name.Contains("Cover")) {
        meshRenderer.material.color = originalColor;
      }
    }
  }
}