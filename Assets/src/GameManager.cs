using System.Collections.Generic;
using DefaultNamespace;
using src.Models;
using UnityEngine;

namespace src {
  public class GameManager : MonoBehaviour {
    public int levelId;
    public List<Team> teams;

    public GameObject scriptHandler;
    private SpawnManager spawnManager;
    private InputManager inputManager;
    private TeamManager teamManager;
    private EventManager eventManager;
    private UiManager uiManager;
    
    private readonly Levels levels = new Levels();
    private Level level;
    private Team currentTeam;

    private bool playerCanAct = true;
    
    private void Start() {
      // Import Managers.
      eventManager = scriptHandler.GetComponent<EventManager>();
      inputManager = scriptHandler.GetComponent<InputManager>();
      spawnManager = scriptHandler.GetComponent<SpawnManager>();
      teamManager = scriptHandler.GetComponent<TeamManager>();
      uiManager = scriptHandler.GetComponent<UiManager>();
      
      // Set up game.
      level = levels.GetLevel(levelId);
      teams = TeamManager.BuildTeams();
      currentTeam = teams[0];
      Debug.Log(currentTeam.Name);
      uiManager.SetTeam(currentTeam);
      
      spawnManager.SpawnSoldiers(level, teams);

      EventManager.OnTurnEnd += ChangeTeam;
      EventManager.OnUnitActing += SetPlayerCanAct;
    }

    private void Update() {
      if (currentTeam != null && currentTeam.IsPlayer && playerCanAct) {
        inputManager.HandleInput();
      }
    }

    private void SetPlayerCanAct(bool isActing) {
      playerCanAct = !isActing;

      if (playerCanAct && currentTeam.CurrentActionPoints == 0) {
        playerCanAct = false;
        eventManager.TurnEnded();
      }
    }

    private void ChangeTeam() {
      Debug.Log("Changing Team");
      eventManager.SetUnitUnusable();
      
      // Perform team cleanup.
      teamManager.ResetActionPoints(currentTeam);
      
      // Change team.
      currentTeam = currentTeam.Id == 1 ? teams[0] : teams[1];
      
      // Update UI.
      uiManager.SetTeam(currentTeam);
    }
  }
}