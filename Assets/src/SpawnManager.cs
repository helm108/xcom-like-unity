﻿using System.Collections.Generic;
using src.Models;
using UnityEngine;

namespace src {
  public class SpawnManager : MonoBehaviour {
    private const float Offset = 0.5f;
    
    public EventManager eventManager;

    public void Start() {
      eventManager = gameObject.GetComponent<EventManager>();
    }
    
    public void SpawnSoldiers(Level level, List<Team> teams) {
      foreach (var team in teams) {
        SpawnTeam(level, team);
      }
    }

    private void SpawnTeam(Level level, Team team) {
      GameObject soldier = Resources.Load(team.SoldierName) as GameObject;

      for (var row = 0; row < level.Map.GetLength(0); row++) {
        for (var col = 0; col < level.Map.GetLength(1); col++) {
          var val = level.Map[row, col];
          var rowFinal = row + Offset;
          var colFinal = col + Offset;

          if (val != team.SpawnTile) continue;
          
          var soldierInstance = Instantiate(
            soldier,
            new Vector3(rowFinal, 0.5f, colFinal),
            Quaternion.identity
          );

          team.Units.Add(soldierInstance);
          ObjectInfo soldierObjectInfo = soldierInstance.GetComponent<ObjectInfo>();
          soldierObjectInfo.team = team;

          eventManager.UnitCreated(soldierObjectInfo);
        }
      }
    }
  }
}