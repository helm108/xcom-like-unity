#if (UNITY_EDITOR)

using DefaultNamespace;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace src {
  public class CustomEditorWindow : EditorWindow {
    private Levels levels = new Levels();
    private int levelId = 2;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Load Level")]
    static void Init() {
      // Get existing open window or if none, make a new one:
      CustomEditorWindow window = (CustomEditorWindow) EditorWindow.GetWindow(typeof(CustomEditorWindow));
      window.Show();
    }

    void OnGUI() {
      GUILayout.Label("Base Settings", EditorStyles.boldLabel);
      levelId = EditorGUILayout.IntField("Level ID", levelId);

      if (GUILayout.Button("Load Level")) {
        var level = levels.GetLevel(levelId);

        GameObject levelContainer = GameObject.Find("Level Container");
        if (levelContainer != null) {
          DestroyImmediate(levelContainer);
        }

        levelContainer = new GameObject("Level Container");
        levelContainer.AddComponent<LevelLoader>();

        LevelLoader levelLoader = levelContainer.GetComponent<LevelLoader>();

        levelLoader.LoadLevel(level, levelContainer);
      }
    }
  }
}

#endif