﻿#if (UNITY_EDITOR)

using System.IO;
using src.Models;
using UnityEditor;
using UnityEngine;

namespace src {
  public class LevelLoader : MonoBehaviour {
    enum Tiles {
      Floor,
      HalfCover,
      FullCover,
      Spawn1,
      Spawn2
    }

    public void LoadLevel(Level level, GameObject levelContainer) {
      GameObject coverFull = Resources.Load("CoverFull") as GameObject;
      GameObject coverHalf = Resources.Load("CoverHalf") as GameObject;
      GameObject tile = Resources.Load("Tile") as GameObject;
      Debug.Log(tile);

      if (coverFull == null || coverHalf == null) {
        throw new System.NullReferenceException("Level Object Missing");
      }

      for (var row = 0; row < level.Map.GetLength(0); row++) {
        for (var col = 0; col < level.Map.GetLength(1); col++) {
          var val = level.Map[row, col];
          var offset = 0.5f;
          var rowFinal = row + offset;
          var colFinal = col + offset;

          switch (val) {
            case (int) Tiles.Floor:
            case (int) Tiles.Spawn1:
            case (int) Tiles.Spawn2:
              var tileObject = Instantiate(
                tile,
                new Vector3(rowFinal, 0, colFinal),
                Quaternion.identity,
                levelContainer.transform
              );
              GameObjectUtility.SetStaticEditorFlags(tileObject, StaticEditorFlags.NavigationStatic);
              break;
            case (int) Tiles.HalfCover:
              Instantiate(
                coverHalf,
                new Vector3(rowFinal, coverHalf.transform.lossyScale.y / 2, colFinal),
                Quaternion.identity,
                levelContainer.transform
              );
              break;
            case (int) Tiles.FullCover:
              Instantiate(
                coverFull,
                new Vector3(rowFinal, coverFull.transform.lossyScale.y / 2, colFinal),
                Quaternion.identity,
                levelContainer.transform
              );
              break;
          }
        }
      }
    }
  }
}

#endif