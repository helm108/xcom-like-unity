namespace src.Models {
  public class Level {
    public int Id;
    public string Name;
    public int[,] Map;
  }
}