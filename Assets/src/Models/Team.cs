using System.Collections.Generic;
using UnityEngine;

namespace src.Models {
  public class Team {
    public int Id;
    public bool IsPlayer;
    public int SpawnTile;
    public string Name;
    public List<GameObject> Units;
    public string SoldierName;
    public int MaxActionPoints;
    public int CurrentActionPoints;
  }
}