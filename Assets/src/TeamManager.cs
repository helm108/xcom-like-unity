using System;
using System.Collections.Generic;
using src.Models;
using UnityEngine;

namespace src {
  public class TeamManager: MonoBehaviour {
    public EventManager eventManager;
    
    public void Start() {
      eventManager = gameObject.GetComponent<EventManager>();

      EventManager.OnUnitCreated += AddActionPoints;
      EventManager.OnSpend += SpendActionPoints;
    }
    
    public static List<Team> BuildTeams() {
      return new List<Team> {
        new Team {
          Id = 0,
          IsPlayer = true,
          SpawnTile = 3,
          Name = "Yellow Team",
          Units = new List<GameObject>(),
          SoldierName = "Soldier1"
        },
        new Team {
          Id = 1,
          IsPlayer = false,
          SpawnTile = 4,
          Name = "Blue Team",
          Units = new List<GameObject>(),
          SoldierName = "Soldier2"
        }
      };
    }
    
    private void SpendActionPoints(Team team, int actionPoints) {
      team.CurrentActionPoints -= actionPoints;
      
      Debug.Log(team.Name + " has " + team.CurrentActionPoints + " points remaining.");
      
      if (team.CurrentActionPoints < 0) {
        throw new System.Exception("Team AP has gone below zero");
      }

      if (team.CurrentActionPoints == 0) {
        
      }
    }

    private void AddActionPoints(ObjectInfo unit) {
      unit.team.MaxActionPoints += unit.maxActionPoints;
      unit.team.CurrentActionPoints = unit.team.MaxActionPoints;
    }

    public int GetActionPoints(Team team) {
      return team.CurrentActionPoints;
    }

    public void ResetActionPoints(Team team) {
      // Reset team total.
      team.CurrentActionPoints = team.MaxActionPoints;

      // Reset individual unit totals.
      foreach (var teamUnit in team.Units) {
        ObjectInfo objectInfo = teamUnit.GetComponent<ObjectInfo>();
        objectInfo.ResetActionPoints();
      }
    }
  }
}