using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace src {
  public class Gun: MonoBehaviour {
    private EventManager eventManager;
    public GameObject bullet;
    private GameObject bulletEmitter;

    private const int BurstCount = 3;
    private const float ConeSize = 0.03f;

    private GameObject owner;
    
    private void Start() {
      eventManager = gameObject.GetComponent<EventManager>();
    }

    public void ShootTarget(GameObject source, GameObject target) {
      owner = source;
      // Face target.
      source.transform.LookAt(target.transform);

      StartCoroutine(Wait(target));
    }

    private IEnumerator Wait(GameObject target) {
      yield return new WaitForFixedUpdate();
      bulletEmitter = owner.transform.Find("BulletEmitter").gameObject;
      var insideWallDetector = bulletEmitter.GetComponent<InsideWallDetector>();

      if (insideWallDetector.isInWall) yield break;
      var selectedObjectInfo = owner.GetComponent<ObjectInfo>();
      selectedObjectInfo.SpendActionPoints(1);
      selectedObjectInfo.SetActing(true);
      selectedObjectInfo.SetState("firing");
      
      Invoke(nameof(Fire), 0);
      Invoke(nameof(Fire), 0.2f);
      Invoke(nameof(Fire), 0.4f);
    }
    
    private void Fire() {
      var position = bulletEmitter.transform.position;
      var rotation = bulletEmitter.transform.rotation;
      
      rotation.x += UnityEngine.Random.Range(-ConeSize, ConeSize);
      rotation.y += UnityEngine.Random.Range(-ConeSize, ConeSize);
      rotation.z += UnityEngine.Random.Range(-ConeSize, ConeSize);

      var bulletInstance = Instantiate(bullet, position, rotation);
      var bulletScript = bulletInstance.GetComponent<Bullet>();
      bulletScript.SetOwner(owner);
      
      // Fire bullet.
      bulletScript.Fire();
    }
  }
}