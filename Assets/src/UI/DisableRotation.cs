using System;
using UnityEngine;

namespace src.UI {
  public class DisableRotation: MonoBehaviour {

    private void LateUpdate() {
      transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 0, 0);
    }
  }
}