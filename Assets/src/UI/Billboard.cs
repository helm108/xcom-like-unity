using System;
using UnityEngine;

namespace src.UI {
  public class Billboard : MonoBehaviour {
    public Camera mainCamera;

    private void LateUpdate() {
      var mainCameraRotation = mainCamera.transform.rotation;
      transform.LookAt(
        transform.position + mainCameraRotation * Vector3.forward,
        mainCameraRotation * Vector3.up
      );
    }
  }
}