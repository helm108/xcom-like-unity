﻿using UnityEngine;
using UnityEngine.AI;

namespace src.Agents {
  public class AgentMovement : MonoBehaviour {
    private NavMeshAgent agent;

    public void Start() {
      agent = GetComponent<NavMeshAgent>();
      agent.updateRotation = false;
    }

    public void LateUpdate() {
      if (agent.velocity.sqrMagnitude > Mathf.Epsilon) {
        agent.transform.rotation = Quaternion.LookRotation(agent.velocity.normalized);
      }
    }
  }
}