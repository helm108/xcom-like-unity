using src.Models;
using UnityEngine;

namespace src {
  public class EventManager: MonoBehaviour {
    // Broadcast a unit spending its action points. 
    public delegate void UnitActionPointEvent(Team team, int actionPoints);
    public static event UnitActionPointEvent OnSpend;
    public void SpendActionPoints(Team team, int actionPointCost) {
      OnSpend?.Invoke(team, actionPointCost);
    }

    // Broadcast a unit's ObjectInfo when it has been created.
    public delegate void UnitCreatedEvent(ObjectInfo unit);
    public static event UnitCreatedEvent OnUnitCreated;
    public void UnitCreated(ObjectInfo unit) {
      OnUnitCreated?.Invoke(unit);
    }
    
    // Broadcast a team ending its turn.
    public delegate void TurnEndedEvent();
    public static event TurnEndedEvent OnTurnEnd;
    public void TurnEnded() {
      OnTurnEnd?.Invoke();
    }

    // Set whether or not a unit is acting.
    public delegate void UnitActingEvent(bool isActing);
    public static event UnitActingEvent OnUnitActing;
    public void SetUnitActing(bool isActing) {
      OnUnitActing?.Invoke(isActing);
    }
    
    // Broadcast Tile MouseOver.
    public delegate void TileMouseOverEvent(GameObject tile);
    public static event TileMouseOverEvent OnTileMouseOver;
    public static void SetCurrentTile(GameObject tile) {
      OnTileMouseOver?.Invoke(tile);
    }

    // Broadcast Tile MouseExit.
    public delegate void TileMouseExitEvent();
    public static event TileMouseExitEvent OnTileMouseExit;
    public static void ClearCurrentTile() {
      OnTileMouseExit?.Invoke();
    }
    
    // Broadcast that selected unit is now unusable.
    public delegate void UnitUnusableEvent();
    public static event UnitUnusableEvent OnUnitUnusable;
    public void SetUnitUnusable() {
      OnUnitUnusable?.Invoke();
    }
    
    // Unit has been shot.
    public delegate void UnitShotEvent(GameObject target);
    public static event UnitShotEvent OnUnitShot;
    public void MarkUnitShot(GameObject target) {
      OnUnitShot?.Invoke(target);
    }
  }
}