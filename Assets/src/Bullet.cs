using System;
using UnityEngine;

namespace src {
  public class Bullet: MonoBehaviour {
    public GameObject scriptHandler;
    private EventManager eventManager;
    private MeshRenderer meshRenderer;
    private Rigidbody rigidbody;
    private GameObject owner;
    private ObjectInfo ownerInfo;
    private const int ForceMultiplier = 10;

    public void Start() {
      eventManager = scriptHandler.GetComponent<EventManager>();
      meshRenderer = gameObject.GetComponent<MeshRenderer>();
      rigidbody = gameObject.GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other) {
      if (other.name.Contains("Cover")) {
        DisableBullet();
      }

      if (!other.name.Contains("Soldier")) return;
      eventManager.MarkUnitShot(other.gameObject);
      DisableBullet();
    }

    private void OnTriggerExit(Collider other) {
      if (!other.name.Equals("WorldContainer")) return;
      DisableBullet();
    }

    private void DisableBullet() {
      meshRenderer.enabled = false;
      rigidbody.Sleep();
    }

    public void Fire() {
      var force = transform.forward * ForceMultiplier;
      var rigidbodyTemp = GetComponent<Rigidbody>();
      rigidbodyTemp.AddForce(force);
      ownerInfo.AddBullet();
    }

    private void OnDestroy() {
      if (ownerInfo == null) return;
      ownerInfo.RemoveBullet();
    }
    
    public void SetOwner(GameObject newOwner) {
      owner = newOwner;
      ownerInfo = owner.GetComponent<ObjectInfo>();
    }
  }
}