using System;
using src.Agents;
using src.Models;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace src {
  public class ObjectInfo : MonoBehaviour {
    public Team team;
    public int maxHealth;
    public int health;
    public int maxActionPoints;
    public int actionPoints;
    public int movementRange = 5;

    public Image healthBar;
    public Image actionPoint1;
    public Image actionPoint2;
    public GameObject scriptHandler;
    public GameObject selectionIndicator;

    private int bulletCount;

    private bool isSelected;
    public bool isActing = false;
    private EventManager eventManager;

    private NavMeshAgent navMeshAgent;

    private string state = "idle";

    public void Start() {
      eventManager = scriptHandler.GetComponent<EventManager>();
      selectionIndicator.SetActive(false);
      navMeshAgent = gameObject.GetComponent<NavMeshAgent>();

      EventManager.OnUnitShot += UnitShot;
    }

    public void Update() {
      if (isActing && state.Equals("moving")) {
        ReachedDestination();
      }
    }

    public void SetState(string newState) {
      state = newState;
    }

    public string GetState() {
      return state;
    }

    private void ReachedDestination() {
      if (navMeshAgent.pathPending || !(navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)) return;
      if (navMeshAgent.hasPath && !(Math.Abs(navMeshAgent.velocity.sqrMagnitude) < 0.01f)) return;
      SetActing(false);
    }

    public void ResetActionPoints() {
      SetActionPoints(maxActionPoints);
    }

    public void SpendActionPoints(int actionPointCost) {
      if (!CanAffordAction(actionPointCost)) {
        throw new Exception("Spending more action points than you have.");
      }
      
      // Updated Unit.
      SetActionPoints(actionPoints - actionPointCost);
      
      // Update Team.
      eventManager.SpendActionPoints(team, actionPointCost);
    }

    public bool CanAffordAction(int actionPointCost) {
      return actionPointCost <= actionPoints;
    }

    private void SetActionPoints(int newActionPoints) {
      actionPoints = newActionPoints;

      switch (actionPoints) {
        case 2:
          actionPoint1.enabled = true;
          actionPoint2.enabled = true;
          break;
        case 1:
          actionPoint1.enabled = true;
          actionPoint2.enabled = false;
          break;
        case 0:
          actionPoint1.enabled = false;
          actionPoint2.enabled = false;
          break;
      }
    }

    public bool CanAct() {
      return actionPoints > 0;
    }

    private void SetHealth(int newHealth) {
      health = newHealth;
      healthBar.fillAmount = (float)health / maxHealth;
      if (newHealth == 0) {
        Die();
      }
    }

    public void SetSelected(bool selected) {
      isSelected = selected;
      selectionIndicator.SetActive(isSelected);
    }

    public void SetActing(bool acting) {
      isActing = acting;
      
      Debug.Log("Unit " + (isActing ? "is" : "is not") + " acting.");

      if (!isActing) {
        SetState("idle");
      }
      
      // Perform local changes.
      if (!isActing && actionPoints == 0) {
        SetSelected(false);
      }

      // If the unit can no longer act, mark it unusable.
      if (!CanAct() && !isActing) {
        eventManager.SetUnitUnusable();
      }
      
      // Broadcast result.
      eventManager.SetUnitActing(acting);
    }

    private void TakeDamage(int damage) {
      var newHealth = health - damage;
      newHealth = newHealth < 0 ? 0 : newHealth;
      SetHealth(newHealth);
    }

    private void UnitShot(GameObject target) {
      if (target == gameObject) {
        TakeDamage(100);
      }
    }

    public void AddBullet() {
      bulletCount++;
    }

    public void RemoveBullet() {
      bulletCount--;
      if (bulletCount == 0) {
        SetActing(false);
      }
    }

    private void Die() {
      Debug.Log("DYING");
      SetState("dead");
      CapsuleCollider ccol = GetComponent<CapsuleCollider>();
      Rigidbody rbody = GetComponent<Rigidbody>();
      NavMeshAgent nma = GetComponent<NavMeshAgent>();
      nma.enabled = false;
      ccol.isTrigger = false;
      rbody.useGravity = true;
      foreach (Transform child in transform) {
        Destroy(child.gameObject);
      }
      rbody.AddForce(transform.forward);
      rbody.AddTorque(transform.forward);
    }
  }
}
