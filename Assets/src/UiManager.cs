using System;
using src.Models;
using UnityEngine;
using UnityEngine.UI;

namespace src {
  public class UiManager: MonoBehaviour {
    
    public Text currentTeamDisplay;
    public Button endTurnButton;
    public EventManager eventManager;
    
    public void Start() {
      eventManager = gameObject.GetComponent<EventManager>();
      endTurnButton.onClick.AddListener(EndTurnClick);
    }

    public void SetTeam(Team team) {
      currentTeamDisplay.text = "Current Team: " + team.Name;
    }

    private void EndTurnClick() {
      eventManager.TurnEnded();
    }
  }
}