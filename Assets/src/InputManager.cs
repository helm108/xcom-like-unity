using System;
using src.Helpers;
using UnityEngine;
using UnityEngine.AI;

namespace src {
  public class InputManager : MonoBehaviour {
    private GameObject selectedObject;
    private NavMeshAgent selectedAgent;
    private ObjectInfo selectedObjectInfo;
    private const int MoveCost = 1;
    private const int ShootCost = 1;
    private const int MoveRange = 4;
    private NavMeshPath pathToTile;
    private bool isPathValid = false;
    private bool redrawLine = true;
    private Gun gun;
    public EventManager eventManager;
    public GameObject currentTile;

    public LineRenderer pathLineRenderer;

    public void Start() {
      eventManager = gameObject.GetComponent<EventManager>();
      gun = gameObject.GetComponent<Gun>();
      
      EventManager.OnUnitUnusable += Deselect;
      EventManager.OnTileMouseOver += SetCurrentTile;
      EventManager.OnTileMouseExit += ClearCurrentTile;
      EventManager.OnUnitActing += ToggleRedrawLine;
    }

    private void ToggleRedrawLine(bool isActive) {
      redrawLine = !isActive;
    }

    private void SetCurrentTile(GameObject tile) {
      if (currentTile != null) return;
      currentTile = tile;
      var meshRenderer = currentTile.GetComponent<MeshRenderer>();
      meshRenderer.material.color = Color.grey;
      
      // Return if there is no source or destination.
      if (currentTile == null || selectedObject == null) return;
      
      // Return if current unit is acting.
      if (selectedObjectInfo.isActing) return;
      
      // Return if target tile is occupied.
      var tileDetector = currentTile.transform.Find("TileDetector");
      if (tileDetector.GetComponent<TileDetector>().isOccupied) return;
      
      // If the path is valid.
      isPathValid = PathToTarget(selectedObject, currentTile);
      
      // Render path, colour tile depending on path validity.
      if (redrawLine) {
        RenderPath(pathToTile, isPathValid);
      }
      
      meshRenderer.material.color = isPathValid ? Color.green : Color.grey;
    }

    private bool PathToTarget(GameObject source, GameObject destination) {
      pathToTile = new NavMeshPath();
      var pathExists = NavMesh.CalculatePath(
        source.transform.position,
        destination.transform.position,
        NavMesh.AllAreas,
        pathToTile
      );

      // Verify path is good.
      if (!pathExists) return false;

      var pathLength = CalculatePathLength.GetPathLength(pathToTile);
      
      // Prevent moving to own tile. 
      if (pathLength < 0.01f) {
        return false;
      }
      return pathLength <= MoveRange;
    }

    private void RenderPath(NavMeshPath path, bool isValid) {
      var corners = path.corners;
      pathLineRenderer.enabled = true;
      pathLineRenderer.startColor = isValid ? Color.green : Color.grey;
      pathLineRenderer.endColor = isValid ? Color.green : Color.grey;
      pathLineRenderer.positionCount = corners.Length;
      pathLineRenderer.SetPositions(corners);
    }

    private void ClearCurrentTile() {
      pathLineRenderer.enabled = false;
      currentTile = null;
      pathToTile = null;
      isPathValid = false;
    }

    public void HandleInput() {
      if (Input.GetMouseButtonDown(0)) {
        SelectUnit();
      }

      if (Input.GetMouseButtonDown(1)) {
        MoveSelectedUnit();
      }
    }

    private void Deselect() {
      Debug.Log("Deselecting");
      if (selectedObjectInfo != null) {
        selectedObjectInfo.SetSelected(false);
      }
      selectedObject = null;
      selectedAgent = null;
      selectedObjectInfo = null;
      pathLineRenderer.enabled = false;
    }

    private void SelectUnit() {
      var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

      if (!Physics.Raycast(ray, out var hit, 100)) return;

      if (hit.collider.CompareTag("Floor")) {
        Deselect();
        Debug.Log("Deselected");
      }
      else if (hit.collider.CompareTag("Selectable")) {
        // If there is no currently selected friendly.
        var targetedObject = hit.collider.gameObject;
        var targetedObjectInfo = targetedObject.GetComponent<ObjectInfo>();

        var isOwnUnit = targetedObjectInfo.team.IsPlayer;
        var isUnitSelected = selectedObject != null;
        var canAct = targetedObjectInfo.CanAct();
        Debug.Log(isOwnUnit + " " + isUnitSelected + " " + canAct);
        // Select unit.
        if (!isUnitSelected && isOwnUnit && canAct) {
          selectedObject = targetedObject;
          selectedObjectInfo = targetedObjectInfo;
          selectedAgent = selectedObject.GetComponent<NavMeshAgent>();
          selectedObjectInfo.SetSelected(true);
        }

        // Deselect unit, select new unit.
        if (isUnitSelected && isOwnUnit) {
          Deselect();
          selectedObject = targetedObject;
          selectedObjectInfo = targetedObjectInfo;
          selectedAgent = selectedObject.GetComponent<NavMeshAgent>();
          selectedObjectInfo.SetSelected(true);
        }

        // Select enemy unit
        if (isUnitSelected && !isOwnUnit) {
          // Gun should have a CanShoot function.
          if (!selectedObjectInfo.CanAffordAction(ShootCost)) return;
          gun.ShootTarget(selectedObject, targetedObject);
        }
      }
    }

    private void MoveSelectedUnit() {
      if (!selectedObject) return;
      
      var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

      // Verify can move.
      if (!Physics.Raycast(ray, out var hit, 100)) return;
      if (!hit.collider.CompareTag("Floor")) return;
      if (!selectedObjectInfo.CanAffordAction(MoveCost)) return;
      if (!isPathValid) return;
      
      selectedObjectInfo.SetState("moving");
      selectedAgent.SetPath(pathToTile);
      selectedObjectInfo.SpendActionPoints(MoveCost);
      selectedObjectInfo.SetActing(true);
    }
  }
}
